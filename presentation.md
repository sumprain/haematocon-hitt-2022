---
theme: solarized
---

HAEMATOCON 2022 # 04 Nov 2022

## Treatment of HIT(T)

<hr>

#### Dr Suman Kumar Pramanik {style="color:green"}

#### Command Hospital (Eastern Command), Kolkata {style="color:gray"}

<hr>

---

## Heparin Induced Thrombocytopenia

#### HIT is a profoundly hypercoagulable state

> Iatrogenic disorder mediated by IgG antibodies that bind with <span style="color: red">**PF4-heparin**</span> complexes {.fragment}

> Causes <span style="color: red">**hypercoagulable state**</span> by activating platelets and procoagulant microparticles {.fragment}

--

> <span style="color: red">**One third to one half**</span> of the patients develop venous, arterial or microvascular thrombosis

> <span style="color: red">**Unfractionated Heparin**</span> is associated with 10-fold increase in risk of HIT as compared with LMWH {.fragment}

---

# Case

---

### Medical History

- 82 years male, DM, Hypertension, Congestive Heart Failure

- Admitted with exacerbation of CHF

- **Subcutaneous unfractionated heparin** (UFH) 5000 IU q12h started on admission date for DVT prophylaxis

- No exposure to heparin in past 3 months

--

### Platelet kinetics

| Date           | D0  | D1  | D3  | D5  | D6                      | D7                                    |
| -------------- | --- | --- | --- | --- | ----------------------- | ------------------------------------- |
| Plt (x 10<sup>9</sup>) | 200 | 220 | 210 | 230 | 150 {style="color:red"} | <span style="color:red">**67**</span> |

--

### Clinical Probability of HIT (4Ts Score)

- **Thrombocytopenia:** > 50% drop (2 pts)

- **Timing of platelet fall:** 6th day (2 pts)

- **Thrombosis or other sequelae:** No thrombosis (0 pt)

- **Other causes of thrombocytopenia:** No other causes discernible (2 pts)

HIGH probability of HIT (Total pts 6) {style="color:red"}

---

### Diagnostic tests for high clinical probability HIT

#### Overview

| HIT Immunoassay Tests <small>_(Detect presence of anti-PF4/heparin antibodies)_</small> | Functional HIT assays <small>_(Detect antibodies capable of binding and activating platelets)_</small> |{style="color:green"}
| ------------------------------------------------------------------------ | --------------------------------------------------------------------------------------- |
| ELISA (detect IgG)                                                       | Serotonin Release Assay (SRA)                                                           |
| ELISA (detect polyspecific antibodies)                                   | Heparin induced platelet activation test (HIPA)                                         |
| IgG specific chemiluminescent assay                                      | Platelet aggregation test (PAT)                                                         |

--

#### Diagnostic tests for our patient

- Immunoassay, and if positive then functional assay

> Likelihood of HIT increases with higher 4Ts score and a higher ELISA OD {style="color:red"}

---

### Management strategy in our patient

- **High probability for HIT** (4Ts score) {style="color:red"}

- HIT ELISA sent, reports pending

- Patient on UFH 5000 U BD

> <span style="color:red">Discontinuation of heparin</span> and initiation of non-heparin anticoagulant at <span style="color:red">therapeutic intensity</span> {.fragment .fade-in}

--

#### Management strategy in patients intermediate probability 4Ts score

> Discontinuation of Heparin {style="color:red}

![](int-prob-mgt.drawio.svg){width=70%}

--

### Role of platelet transfusion

> For patients with HIT who are at **average bleeding risk**, <span style="color:red">routine platelet transfusion not recommended</span>

--

### HIT Laboratory Test Results

- HIT ELISA OD = <span style="color:red">1.8 (> 0.4)</span>

- Sample sent for confirmatory functional assay (SRA)

- Patient remains clinically stable with no symptoms or signs of PE, DVT or arterial thrombosis

---

### Diagnostic algorithm till now

![](diag-algo-hznt.drawio.svg)

---

#### Patient has acute isolated HIT (without thrombosis), platelets 67000/cu mm

> VKA not to be initiated prior to platelet count recovery (platelets$\geq$150000/$\mu$L) {style="color:red"}

**Early initiation** of VKA associated with <span style="color:red">**thrombosis**</span> {.fragment .fade-in}

--


### Acute HIT or HITT

> Discontinuation of heparin and initiation of non-heparin anticoagulants at therapeutic doses

**Argatroban, bivalirudin, danaparoid, <span style="color:red">fondaparinux or DOAC</span>**

--

#### Rationale of anticoagulant selection

- Using non-heparin anticoagulant (compared with stopping heparin +/- starting VKA) associated with 
**fewer thrombotic events**, but probably increased bleeding risk.

- No direct comparison with DOACs and parenteral anticoagulants

- Small number of patients treated with DOACs in case series had fewer thrombotic events and benefits and harms **compare favorably 
to parenteral agents**

--

### Clinical context and anticoagulation

| Clinical context                                                          | Implications of Anticoagulant selection                                                                                                        |
| ------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| Critical illness<br>Increased bleeding risk<br>Possible urgent procedures | **Argatroban** or **Bivalirudin** (Shorter duration of effect)<br>Avoid or reduce dose of Argatroban if moderate to severe hepatic dysfunction |

--

| Clinical context                                                  | Implications of anticoagulant selection           |
| ----------------------------------------------------------------- | ------------------------------------------------- |
| Life or limb threatening VTE<br>(massive PE or venous limb gangrene) | Parenteral non-heparin anticoagulant preferred (Argatroban, Bivalirudin, Danaparoid, **Fondaparinux**)<br>_Fewer such patients treated with DOACs_ |

--

| Clinical context                                    | Implications of Anticoagulant selection                                                         |
| --------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| Clinically stable patients at average bleeding risk | **Fondaparinux** or **DOACs** reasonable<br>Most published DOAC experience with **Rivaroxaban** |

--

### Non-heparin anticoagulants

#### Fondaparinux (direct Xa inhibitor), SC

- < 50kg $\rightarrow$ 5 mg daily; $\geq$ 100kg $\rightarrow$ 7.5 mg daily

- Renal clearance

- No monitoring

--

#### Rivaroxaban (direct Xa inhibitor), PO

- **HITT:** 15 mg twice daily x 3 weeks, then 20 mg daily

- **HIT:** 15 mg twice daily until platelet count recovery ($\geq$ 150000/$\mu$L)

- Renal clearance

- No monitoring

--

### Our patient

- Discontinue UFH and start on Rivaroxaban 15 mg twice daily

- Next 8 days, platelet count rises from 67000 to 167000/$\mu$L and no evidence of bleeding

--

### Screening for VTE in asymptomatic patients with HIT

> <span style="color:red">**Bilateral lower extremity compression US**</span> to screen for asymptomatic proximal DVT

<small>US studies have identified silent LL DVT in 12 - 44% of asymptomatic patients with HIT</small>

> <span style="color:red">**Upper extremity US**</span> only in patients with upper limb CVC to screen for asymptomatic 
DVT {.fragment .fade-in}

--

### Our patient

- **Occlusive left popliteal vein DVT.** Continues with rivaroxaban 15 mg twice a day for 3 weeks, then rivaroxaban 20 mg once a day for 
a total of 3 months

- At 3 months, platelet counts 205000/$\mu$L, health same at baseline. **Stop rivaroxaban**

--

### After 15 months

- CHF with severe aortic stenosis (aortic valve area 0.6 cm^2^)

- Planned for **aortic valve replacement**

--

#### H/O HITT, requires open heart surgery with intraoperative anticoagulation

- Platelet counts 206000/$\mu$L

- **HIT ELISA: OD 0.2** (normal < 0.4) {style="color:red"}

--

### Five phases of HIT

| Phase              | Platelet count | Immunoassay | Functional assay |
| ------------------ | :------------: | :---------: | :--------------: |
| **Suspected HIT**  |  $\downarrow$  |      ?      |        ?         |
| **Acute HIT**      |  $\downarrow$  |      +      |        +         |

--

| Phase              | Platelet count | Immunoassay | Functional assay |
| ------------------ | :------------: | :---------: | :--------------: |
| **Subacute HIT A** |     Normal     |      +      |        +         |
| **Subacute HIT B** |     Normal     |      +      |        -         |
| **Remote HIT**     |     Normal     |      -      |        -         |

--

#### Subacute HIT B or remote HIT requiring cardiovascular surgery

> **Intraoperative anticoagulation with Heparin** {style="color:red"}

<small>Treatment with heparin limited to **intraoperative setting**</small>

<small>**Postoperative platelet count monitoring** for HIT may be necessary</small>

---

## To conclude ...

- High degree of clinical suspicion is required for the diagnosis

- Clinical stratification to be done with 4Ts score

- Immunoassay, followed by if required functional assay needs to be done

- One of the causes of thrombocytopenia with prothrombotic state

--

> Immediate discontinuation of heparin followed by appropriate non-heparin / non VKA anticoagulation in 
therapeutic dose is the hallmark of treatment

---

## THANK YOU

<small>http://www.hematology.org/VTEguidelines</small>
